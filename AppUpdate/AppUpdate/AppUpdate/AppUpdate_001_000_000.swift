//
// Created by Marko Cicak on 11/17/17.
// Copyright (c) 2017 codecentric AG. All rights reserved.
//

import Foundation

class AppUpdate_001_000_000: NSObject, AppUpdate {

    func stepName() -> String {
        return "upd-\(version())"
    }

    func version() -> String {
        return "001-000-000"
    }

    func canExecuteUpdate() -> Bool {
        return true
    }

    func stepDescription() -> String {
        return "Purge keychain"
    }

    func updateBlock() -> (() -> Void) {
        return {
            // EXAMPLE: delete keychain data if any exists
        }
    }
}
