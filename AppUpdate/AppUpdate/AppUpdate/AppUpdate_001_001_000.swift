//
// Created by Marko Cicak on 11/17/17.
// Copyright (c) 2017 codecentric AG. All rights reserved.
//

import Foundation

class AppUpdate_001_001_000: NSObject, AppUpdate {
    func version() -> String {
        return "001-001-000"
    }

    func canExecuteUpdate() -> Bool {
        if #available(iOS 9.0, *) {
            return true
        } else {
            return false
        }
    }

    func stepName() -> String {
        return "upd-\(version())"
    }

    func stepDescription() -> String {
        return "Index documents and photos in spotlight."
    }

    func updateBlock() -> (() -> Void) {
        return {
            // EXAMPLE: iterate through all documents and photos and index them in spotlight
        }
    }
}
