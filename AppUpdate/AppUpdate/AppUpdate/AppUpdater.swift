//
// Created by Marko Cicak on 11/17/17.
// Copyright (c) 2017 codecentric AG. All rights reserved.
//

import Foundation

@objc protocol AppUpdate {
    // Version in format ###-###-### with leading zeros. Used to sort updaters chronologically.
    func version() -> String

    // Could be that update is only for specific ios version, but should not be executed otherwise
    func canExecuteUpdate() -> Bool

    // String in format upd-#_#_#. Needs to be unique across all app updaters.
    func stepName() -> String

    // Description is stored as value for stepName key in StandardUserDefaults
    func stepDescription() -> String

    func updateBlock() -> (() -> Void)
}

class AppUpdater {

    class func performUpdateSteps() {
        let updateClasses = getClassesImplementingProtocol(p: AppUpdate.self)
        let updaters = updateClasses.map({ (updaterClass) -> AppUpdate in
            return updaterClass.alloc() as! AppUpdate
        }).sorted {
            $0.version() < $1.version()
        }

        for updater in updaters {
            if updater.canExecuteUpdate() {
                performUpdateStepWithUpdater(updater)
            }
        }
        UserDefaults.standard.synchronize()
    }

    class func performUpdateStepWithUpdater(_ updater: AppUpdate) {
        if (UserDefaults.standard.object(forKey: updater.stepName()) == nil) {
            print("▸ Performing update step \(updater.stepName())")
            updater.updateBlock()()
            UserDefaults.standard.setValue(updater.stepDescription(), forKey: updater.stepName())
            print("▸ Finished update step: \(updater.stepName())")
        }
    }
}
