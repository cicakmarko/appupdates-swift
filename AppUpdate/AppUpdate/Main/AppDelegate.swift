//
//  AppDelegate.swift
//  AppUpdate
//
//  Created by Marko Cicak on 11/17/17.
//  Copyright © 2017 codecentric AG. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
#if (arch(i386) || arch(x86_64)) && os(iOS)
        let dirs: [String] = NSSearchPathForDirectoriesInDomains(FileManager.SearchPathDirectory.documentDirectory,
                FileManager.SearchPathDomainMask.allDomainsMask, true)
        print(dirs[dirs.endIndex - 1])
#endif

        AppUpdater.performUpdateSteps()
        return true
    }
}
